<?php
$text = [
    'home' => [
        'title' => 'PhD computer science student',
        'title_hook' => "I want to do a CIFRE PhD in computer science in September 2024, I'm looking for a company that can offer me a subject oriented towards complexity or image processing problems",
        'info_name' => "Etienne Collet<br/>21 years",
        'info_mail' => "etienne02.collet@gmail.com",
        'info_phone' => "+33 06 18 62 41 33",
        'info_address' => "Noisy-le-Grand 93160, France",
        'info_linkedin' => "Linkedin",
        'info_github' => "Github",

        'info_skills_title' => "SKILLS",
        'info_skills_sub_title_one' => "WEB :",
        'info_skills_sub_info_one' => "HTML, CSS, PHP, JS, Laravel, SLIM, REACT",
        'info_skills_sub_title_two' => "Software :",
        'info_skills_sub_info_two' => "C, C++, Python, JAVA",
        'info_skills_sub_title_three' => "Database :",
        'info_skills_sub_info_three' => "SQL",
        'info_skills_sub_title_four' => "Other :",
        'info_skills_sub_info_four' => "YACC, BISON, LATEX",

        'info_skills_office' => "Mastery of office automation tools",
        'info_skills_driver_license' => "Holder of a B driving licence (vehicle)",

        'info_langage_title' => "LANGUAGE",
        'info_langage_one' => "French ( Native language )",
        'info_langage_two' => "Technical English",
        'info_langage_two_download' => "Download<br/>The Certification",

        'info_hobbies_title' => "HOBBIES",
        'info_hobbies_one' => "Chess",
        'info_hobbies_two' => "Climbing",
        'info_hobbies_three' => "Fishing",
        'info_hobbies_four' => "LEGO",
        'info_hobbies_five' => "Mountain biking",
        'info_hobbies_six' => "",

        'info_other_xp_title' => "Other Expériences",
        'info_other_xp_one_title' => "February - MaY 2022: Mc Donald’s",
        'info_other_xp_one' => "Polyvalent team member",
        'info_other_xp_two_title' => "2018 - 2020 : Grimpe à Noisy",
        'info_other_xp_two' => "Climbing teacher<br/>Children's groups from 12 to 16 years",
        
        'formation_title' => "Trainings :",
        'formation_one' => "2024 – 2027 Computer science thesis",
        'formation_two' => "2022 – 2024 Master's 2 in Computer Science<br/>
            Institut d’électronique et d’informatique Gaspard-Monge (IGM)<br/>
            Image specialisation in the second year",
        'formation_bourse' => "2023 Awarded the Labex Bézout merit scholarship",
        'formation_three' => "2019 – 2022 Mathematics - Computer Science degree<br/>
            Gustave Eiffel University ( Mention Bien )",

        'project_title' => "Projects :",
        'project_one_title' => "January 2023 – October 2023 : IGM",
        'project_one_subject' => "Research internship, subject : ",
        'project_one_question' => "Distinct shortest walk enumeration",
        'project_one_context_title' => "Context :",
        'project_one_context' => "Discovery of research, supervised by 4 researchers",
        'project_one_realization_title' => "Production :",
        'project_one_realization_one' => "Write theoretical proofs of algorithms",
        'project_one_realization_two' => "Implementation of an innovative algorithm for data graphs",
        'project_one_realization_three' => "Discussions and exchanges on the theses",
        'project_one_realization_four' => "",
        'project_one_technical_title' => "Technical environment :",
        'project_one_technical' => "Use of Python and NetworkX for proofs of concept",

        'project_two_title' => "October 2021 – June 2022 : IGM",
        'project_two_subject' => "Tutored project, subject : ",
        'project_two_question' => "Complex phase portraits",
        'project_two_context_title' => "Context :",
        'project_two_context' => "Carrying out optimisation on a Python application with a high degree of autonomy",
        'project_two_realization_title' => "Production :",
        'project_two_realization_one' => "Setting up a profiler to study an unconventional algorithm",
        'project_two_realization_two' => "Created a C library to get round the slowness of Python",
        'project_two_realization_three' => "Performance testing with large data sets",
        'project_two_realization_four' => "",
        'project_two_technical_title' => "Technical environment :",
        'project_two_technical' => "Based on a python application <br/> abstraction of display functions to time calculation functions",
        
        'xp_title' => "Expérences :",
        'xp_one_title' => "August 2022 - September 2024 : Globalis Média Système",
        'xp_one_subject' => "Function : ",
        'xp_one_function' => "development engineer (work-study)",
        'xp_one_context_title' => "Context :",
        'xp_one_context' => "Web development company founded in 1997 based on very strong technical expertise in PHP",
        'xp_one_realization_title' => "Production :",
        'xp_one_realization_one' => "Correction, development and creation of web applications",
        'xp_one_realization_two' => "Algorithm creation with a particular focus on optimisation and technical rigour",
        'xp_one_realization_three' => "Examples of websites: e-certif, Rallye clés en main or Oqali",
        'xp_one_realization_four' => "",
        'xp_one_technical_title' => "Technical environment :",
        'xp_one_technical' => "Very high technical requirements on Slim, Laravel, React and SQL with 6 or 7 simultaneous projects",

        'xp_two_title' => "September 2021 et 2023 : IGM",
        'xp_two_subject' => "Tutoring in Computer Science and Mathematics",
        'xp_two_function' => "Providing revision courses for first-year students",
    ],
    'pp' => [
        'title_one' => "Setting up my server",
        'discribe_one' => "You're looking at it right now. When I wanted to set up my own my own website, 
            I decided not to use a hosting company. After a long phase of research of research, I salvaged 
            an old DELL computer and got it up and running again. Once it was functional, I removed Windows 
            to install a LAMP base. After that, I switched over to the network. A complicated part that 
            allowed me to apply my L3 courses. Once I was open to the world, I started implementing my 
            website with PHP, HTML, CSS and JS. I paid particular attention to making it responsive. 
            However, there are still However, there are still a few errors, so this one is constantly 
            evolving.",

        'title_two' => "Snake in 3D",
        'discribe_two' => "With the intention of discovering the mathematics behind a three-dimensional 
            three-dimensional display, this project implements the famous game SNAKE with a first-person 
            first-person view. So, during my two-week L2 Christmas vacation, I took the opportunity to 
            code this little game. to code this little game. I started by implementing the game in two 
            dimensions. During this first phase, I made sure that the game was scalable, so that later it 
            could be transformed into a mini map. that it could later be transformed into a mini-map. 
            I then began implementing the game in 3D by defining a vanishing point.  defining a vanishing 
            point. Next, displaying the grid on the ground was one of the most with many calculation errors. 
            I also implemented the display walls and the apple. To finish the project, I displayed the 
            snake's body, which was also was also a tricky step, as initially I wanted it to be tubular. 
            However, I couldn't get the rendering right and settled on a rectangular body. Afterwards, 
            I didn't have time to implement the transitions, as classes resumed.",
    ],
    'tp' => [
        'top_bar_one' => "Database",
        'top_bar_two' => "Phase Portrait",
        'title_one' => "Enumerating the shortest paths in a data graph",
        'discribe_one' => "This presentation in French introduced me to the world of research, which in 
            turn my desire to do a PhD. It deals with the following problem: how can enumerate the shortest 
            distinct paths in a data graph? graph? So we're taking a mainly theoretical approach to the 
            subject. We start with a few reminders, such as the Cartesian product, languages, automata and 
            data graphs. We then express the queries we're going to propose and how they work. To conclude 
            this section, we present the complexity that will enable us to evaluate our algorithm. 
            For continue with the body of the project, the presentation of our algorithm with pseudo code, 
            proof of concept and proof of correctness. Finally, this presentation concludes with the 
            various prospects for continuing the project. A project I'm very proud to present to you, 
            and hope you enjoy discovering it. discover.",
        'title_two' => "Phase Portrait",
        'discribe_two' => "This tutored project, which took place during my last year of undergraduate 
            studies, concerned the generation of Phase Portraits. The aim of the project was to : to 
            optimize the calculation and generation of Phase Portraits. This presentation begins by 
            recalling what a Phase Portrait is and what techniques are used in our case to generate 
            them. used in our case to generate them. This is followed by an introduction to the various 
            technologies we used to optimize the code. Finally, I'll outline the changes made to the 
            code and the results I obtained. Initially, the code was relatively naive, but we still 
            achieved 15 times better acceleration better acceleration at the end of the project. 
            In this document, we end with a list of conclusions and possible possible improvements, 
            but as I continued the project in parallel, these may have already been already been 
            addressed. The new report with the various new improvements is is currently being drafted."
    ],
    'up' => [

    ],
    'other' => [
        'title_one' => "Important documents",
        'paper_one' => "Note-taking L1",
        'paper_two' => "Note-taking L2",
        'paper_three' => "Note-taking L3",
        'paper_four' => "Note-taking M1",
        'paper_five' => "Certificate of merit scholarship",
    ],
    'generic' => [
        'download' => "Download",
        'french' => "French",
        'english' => "English",
        'home' => "Home",
        'pp' => "Personnal Project",
        'up' => "University Project",
        'tp' => "Tutored Project",
        'other' => "Other",
    ],
];