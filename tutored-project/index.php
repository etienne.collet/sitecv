<!DOCTYPE html>
<html lang="fr">

<head>
    <link rel="stylesheet" href="./tutored-project/style.css">
</head>

<div class="container-page">
    <div class="container-topbar">
        <div class="topbar">
            <button onclick="window.scroll(0, getOffsetTop(document.getElementById('tutored-title_one')) - 80);">
                <?php echo($text['tp']['top_bar_one'])?>
            </button>
            <button onclick="window.scroll(0, getOffsetTop(document.getElementById('tutored-title_two')) - 80);">
                <?php echo($text['tp']['top_bar_two'])?>
            </button>
        </div>
    </div>
    <div class="container-tutored">
        <h2 id="tutored-title_one" class="tutored-title">
            <?php echo($text['tp']['title_one'])?>
        </h2>
        <div class="container-grid">
            <div class="container-left">
                <div>
                    <h3>
                        <?php echo($text['generic']['teacher'])?>
                    </h3>
                    <div class="list-teacher">
                        <div class="teacher">
                            <?php echo($text['generic']['claireDavid'])?>
                        </div>
                        <div class="teacher">
                            <?php echo($text['generic']['olivierCure'])?>
                        </div>
                        <div class="teacher">
                            <?php echo($text['generic']['victorMarsaut'])?>
                        </div>
                        <div class="teacher">
                            <?php echo($text['generic']['nadimeFrancis'])?>
                        </div>
                    </div>
                </div>
                <div class="container-discribe">
                    <h3 style="font-size:20px">
                        <?php echo($text['generic']['discribe'])?>
                    </h3>
                    <p style="font-size:18px">
                        <?php echo($text['tp']['discribe_one'])?>
                    </p>
                </div>
            </div>
            <div class="container-rigth">
                <div class="container-display">
                    <embed src="./.media/enum_shortest_walk.pdf" width="580" height="530" type="application/pdf" class="display-doc"/>
                </div>
            </div>
        </div>
    </div>

    <div class="container-tutored">
        <h2 id="tutored-title_two" class="tutored-title">
            <?php echo($text['tp']['title_two'])?>
        </h2>
        <div class="container-grid">
            <div class="container-left">
                <div>
                    <h3>
                        <?php echo($text['generic']['teacher'])?>
                    </h3>
                    <div class="list-teacher">
                        <div class="teacher">
                            <?php echo($text['generic']['olivierBouillot'])?>
                        </div>
                    </div>
                </div>
                <div class="container-discribe">
                    <h3 style="font-size:20px">
                        <?php echo($text['generic']['discribe'])?>
                    </h3>
                    <p style="font-size:18px">
                        <?php echo($text['tp']['discribe_two'])?>
                    </p>
                </div>
            </div>
            <div class="container-rigth">
                <div class="container-display">
                    <embed src="./.media/RapportProjetTut.pdf" width="600" height="500" type="application/pdf" class="display-doc"/>
                </div>
            </div>   
        </div> 
    </div>
</div>

<script>
    function getOffsetTop( elem ) {
        console.log(elem);
        var offsetTop = 0;
        do {
            if ( !isNaN( elem.offsetTop ) ) {
                offsetTop += elem.offsetTop;
            }
        } while( elem = elem.offsetParent );
        console.log(offsetTop);
        return offsetTop;
    }
    //document.documentElement.scrollTop a chercher pour le défilement
</script>

</html>
