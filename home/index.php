<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="./style.css" type="text/css">
</head>
<a href="./.media/CV_these23_10_2.pdf" download="CV-2023" class="download-button">
    <?php echo($text['generic']['download'])?>
</a>
<div id="cv">
    <div class="head-container">
        <h1><?php echo($text['home']['title'])?></h2>
        <p>
            <?php echo($text['home']['title_hook'])?>
        </p>
    </div>

    <div class="line-one-left-container">
        <img id="face" src="./.media/face.png">
        <div class="line"></div>
        <div class="info-container">
            <h3><?php echo($text['home']['info_name'])?></h3>
            <!--
                <a href="tel:0618624133">06 18 62 41 33</a>
                <a href="mail:etienne02.collet@gmail.com">etienne02.collet@gmail.com</a>
            -->
            <p><?php echo($text['home']['info_mail'])?></p>
            <p><?php echo($text['home']['info_phone'])?></p>
            <p><?php echo($text['home']['info_address'])?></p>
            <a target="_blank" href="https://linkedin.com/in/etienne-collet215205" class="botton-link"><?php echo($text['home']['info_linkedin'])?></a>
            <a target="_blank" href="https://github.com/etinncollet" class="botton-link"><?php echo($text['home']['info_github'])?></a>
        </div>
        <div class="line-big"></div>
    </div>
    <div class="line-one-rigth-container">
        <div>
            <div class="title-rigth-container">
                <h3 class="title-rigth"><?php echo($text['home']['formation_title'])?></h3>
            </div>
            <div class="project-container">
                <ul>
                    <!--<li>
                        <h4><?php echo($text['home']['formation_one'])?></h4>
                    </li>-->
                    <li>
                        <h4><?php echo($text['home']['formation_two'])?><h4>
                    </li>
                    <li>
                        <h4><?php echo($text['home']['formation_bourse'])?><h4>
                    </li>
                    <li>
                        <h4><?php echo($text['home']['formation_three'])?><h4>
                    </li>
                </ul>
            </div>
        </div>
        <div>
            <div class="title-rigth-container">
                <h3 class="title-rigth">
                    <?php echo($text['home']['project_title'])?>
                </h3>
            </div>
            <div>
                <div class="project-container">
                    <h4><?php echo($text['home']['project_one_title'])?></h4>
                    <div class="headline">
                        <div>
                            <?php echo($text['home']['project_one_subject'])?>
                        </div>
                        <p style="text-decoration: underline; margin: 0;">
                            <?php echo($text['home']['project_one_question'])?>
                        </p>
                    </div>
                    <div class="headline">
                        <h5>
                            <?php echo($text['home']['project_one_context_title'])?>
                        </h5>
                        <p>
                            <?php echo($text['home']['project_one_context'])?>
                        </p>
                    </div>

                    <h5>
                        <?php echo($text['home']['project_one_realization_title'])?>
                    </h5>
                    <ul id="noMargin">
                        <li>
                            <?php echo($text['home']['project_one_realization_one'])?>
                        </li>
                        <li>
                        <?php echo($text['home']['project_one_realization_two'])?>
                        </li>
                        <li>
                        <?php echo($text['home']['project_one_realization_three'])?>
                        </li>
                    </ul>

                    <div class="headline">
                        <h5>
                            <?php echo($text['home']['project_one_technical_title'])?>
                        </h5>
                        <p>
                            <?php echo($text['home']['project_one_technical'])?>
                        </p>
                    </div>
                </div>

                <div class="project-container">
                    <h4><?php echo($text['home']['project_two_title'])?></h4>
                    <div class="headline">
                        <div>
                            <?php echo($text['home']['project_two_subject'])?>
                        </div>
                        <p style="text-decoration: underline; margin: 0;">
                            <?php echo($text['home']['project_two_question'])?>
                        </p>
                    </div>
                    <div class="headline">
                        <h5>
                            <?php echo($text['home']['project_two_context_title'])?>
                        </h5>
                        <p>
                            <?php echo($text['home']['project_two_context'])?>
                        </p>
                    </div>

                    <h5>
                        <?php echo($text['home']['project_two_realization_title'])?>
                    </h5>
                    <ul id="noMargin">
                        <li>
                            <?php echo($text['home']['project_two_realization_one'])?>
                        </li>
                        <li>
                        <?php echo($text['home']['project_two_realization_two'])?>
                        </li>
                        <li>
                        <?php echo($text['home']['project_two_realization_three'])?>
                        </li>
                    </ul>

                    <div class="headline">
                        <h5>
                            <?php echo($text['home']['project_two_technical_title'])?>
                        </h5>
                        <p>
                            <?php echo($text['home']['project_two_technical'])?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="line-two-left-container">
        <div class="line-big"></div>
        <h3>
            <?php echo($text['home']['info_skills_title'])?>
        </h3>
        <div class="info-skill-container">
            <div>
                <h4>
                    <?php echo($text['home']['info_skills_sub_title_one'])?>
                </h4>
                <?php echo($text['home']['info_skills_sub_info_one'])?>
                <h4>
                    <?php echo($text['home']['info_skills_sub_title_two'])?>
                </h4>
                <?php echo($text['home']['info_skills_sub_info_two'])?>
                <h4>
                    <?php echo($text['home']['info_skills_sub_title_three'])?>
                </h4>
                <?php echo($text['home']['info_skills_sub_info_three'])?>
                <h4>
                    <?php echo($text['home']['info_skills_sub_title_four'])?>
                </h4>
                <?php echo($text['home']['info_skills_sub_info_four'])?>
            </div>
            <p><?php echo($text['home']['info_skills_office'])?></p>
            <p><?php echo($text['home']['info_skills_driver_license'])?></p>
        </div>
        <div class="line"></div>
        <div class="info-language-container">
            <h3>
                <?php echo($text['home']['info_langage_title'])?>
            </h3>
            <p style="margin-bottom: 6px;">
                <?php echo($text['home']['info_langage_one'])?>
            </p>
            <p style="margin-bottom: 16px;">
                <?php echo($text['home']['info_langage_two'])?>
            </p>
            <?php require_once("./home/save.php"); ?>
        </div>
        <div class="line"></div>
        <div class="info-loisir-container">
            <h3><?php echo($text['home']['info_hobbies_title'])?></h3>
            <div class="loisir-grid-container">
                <p><?php echo($text['home']['info_hobbies_one'])?></p>
                <p><?php echo($text['home']['info_hobbies_two'])?></p>
                <p><?php echo($text['home']['info_hobbies_three'])?></p>
                <p><?php echo($text['home']['info_hobbies_four'])?></p>
                <p><?php echo($text['home']['info_hobbies_five'])?></p>
                <p><?php echo($text['home']['info_hobbies_six'])?></p>
            </div>
        </div>
        <div class="line"></div>
        <div class="info-loisir-container">
            <h3>
                <?php echo($text['home']['info_other_xp_title'])?>
            </h3>
            <div>
                <h4>
                    <?php echo($text['home']['info_other_xp_one_title'])?>
                </h4>
                <div>
                <?php echo($text['home']['info_other_xp_one'])?>
                </div>
            </div>
            <div>
                <h4>
                    <?php echo($text['home']['info_other_xp_two_title'])?>
                </h4>
                <div>
                    <?php echo($text['home']['info_other_xp_two'])?>
                </div>
            </div>
        </div>
    </div>
    <div class="line-two-rigth-container">
    <div>
            <div class="title-rigth-container">
                <h3 class="title-rigth">
                    <?php echo($text['home']['xp_title'])?>
                </h3>
            </div>
            <div>
                <div class="project-container">
                <h4><?php echo($text['home']['xp_one_title'])?></h4>
                    <div class="headline">
                        <div>
                            <?php echo($text['home']['xp_one_subject'])?>
                        </div>
                        <p style="text-decoration: underline; margin: 0;">
                            <?php echo($text['home']['xp_one_function'])?>
                        </p>
                    </div>
                    <div class="headline">
                        <h5>
                            <?php echo($text['home']['xp_one_context_title'])?>
                        </h5>
                        <p>
                            <?php echo($text['home']['xp_one_context'])?>
                        </p>
                    </div>

                    <h5>
                        <?php echo($text['home']['xp_one_realization_title'])?>
                    </h5>
                    <ul id="noMargin">
                        <li>
                            <?php echo($text['home']['xp_one_realization_one'])?>
                        </li>
                        <li>
                            <?php echo($text['home']['xp_one_realization_two'])?>
                        </li>
                        <li>
                            <?php echo($text['home']['xp_one_realization_three'])?>
                        </li>
                    </ul>

                    <div class="headline">
                        <h5>
                            <?php echo($text['home']['xp_one_technical_title'])?>
                        </h5>
                        <p>
                            <?php echo($text['home']['xp_one_technical'])?>
                        </p>
                    </div>
                </div>
                <div class="project-container">
                    <h4>
                        <?php echo($text['home']['xp_two_title'])?>
                    </h4>
                    <?php echo($text['home']['xp_two_subject'])?>
                    <br/>
                    <?php echo($text['home']['xp_two_function'])?>
                </div>
            </div>
        </div>
    </div>
</div>

</html>