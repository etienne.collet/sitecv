<?php
require_once("dev.php");

$servername = $dbData['servername'];
$username = $dbData['username'];
$password = $dbData['password'];
$dbname = $dbData['dbname'];

// Create connection
$conn = new mysqli($servername, $username, $password);

// Check connection
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

$sql = "CREATE DATABASE IF NOT EXISTS $dbname";
if ($conn->query($sql) !== TRUE) {
  echo "Error creating database: " . $conn->error;
}

$conn->close(); 
$conn = new mysqli($servername, $username, $password, $dbname);

$sql = "CREATE TABLE IF NOT EXISTS app_connections (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    key_user INT(6) NOT NULL,
    date_entry TIMESTAMP NOT NULL,
    page_id VARCHAR(100) NOT NULL,
    lang VARCHAR(30) NOT NULL
)";
if ($conn->query($sql) !== TRUE) {
    echo "Error creating table app_connections : " . $conn->error;
    die();
}

function save ($session, $page, $lang) {
    $sql = "INSERT INTO app_connections (key_user, date_entry, page_id, lang)
VALUES (812801, NOW(), '$page', '$lang')";
    global $conn;
    if ($conn->query($sql) !== TRUE) {
        echo "Error: " . $sql . "<br>" . $db->error;
        die();
    }
}

